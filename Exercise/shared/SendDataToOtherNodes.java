public class SendDataToOtherNodes implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<DataApi.DataItemResult> {
    private final String TAG = this.getClass().getSimpleName();
    private final GoogleApiClient googleApiClient;
    private NotificationData notificationData;
    private IntervalData intervalData;
    private DataType dataType;

    public SendDataToOtherNodes(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void execute() {
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        // TODO: Create DataItem
        // Wearable.DataApi.putDataItem(googleApiClient, dataRequest).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: Reason " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    @Override
    public void onResult(DataApi.DataItemResult dataItemResult) {
        Log.d(TAG, "onResult: " + dataItemResult.getStatus());
        if (!dataItemResult.getStatus().isSuccess()) {
            Log.e(TAG, "onResult: Failed to put dataItem " + dataItemResult);
        }

        googleApiClient.disconnect();
    }
}
