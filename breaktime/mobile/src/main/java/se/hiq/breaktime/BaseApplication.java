package se.hiq.breaktime;

import android.content.SharedPreferences;

/**
 * Created by alexso on 14/03/15.
 */
public class BaseApplication extends android.app.Application {

    public static BaseApplication APPLICATION;
    private final String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_NAME";
    Setting setting;

    @Override
    public void onCreate() {
        super.onCreate();
        APPLICATION = this;
        SharedPreferences preferences = getSharedPreferences(SHARED_PREFERENCES_NAME, getBaseContext().MODE_PRIVATE);
        setting = new Setting(preferences);
    }

    Setting getSetting() {
        return setting;
    }
}
