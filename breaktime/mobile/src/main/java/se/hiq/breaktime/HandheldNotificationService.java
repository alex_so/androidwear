package se.hiq.breaktime;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import se.hiq.shared.Constants;
import se.hiq.shared.DeleteDataItemCommand;

/**
 * Created by alexso on 15/03/15.
 */
public class HandheldNotificationService extends WearableListenerService {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().startsWith(Constants.DISABLE_NOTIFICATIONS_PATH)) {
            BaseApplication.APPLICATION.getSetting().saveInterval(-1);
            Toast toast = Toast.makeText(BaseApplication.APPLICATION, getString(R.string.disabled_from_wear), Toast.LENGTH_SHORT);
            toast.show();
            Intent intent = new Intent(BroadcastEvents.INTERVAL_TIME_UPDATED);
            LocalBroadcastManager.getInstance(BaseApplication.APPLICATION).sendBroadcast(intent);

        }
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent dataEvent : dataEvents) {
            Log.d(TAG, "onDataChanged: " + dataEvent);

            String path = dataEvent.getDataItem().getUri().getPath();
            if (dataEvent.getType() == DataEvent.TYPE_DELETED) {
            } else if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                if (path.startsWith(Constants.SET_INTERVAL_PATH)) {
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    long timerTimestamp = dataMapItem.getDataMap().getLong(Constants.KEY_NOTIFICATION_TIMESTAMP);
                    int minutes = dataMapItem.getDataMap().getInt(Constants.KEY_SET_INTERVAL_VALUE, -1);
                    BaseApplication.APPLICATION.getSetting().saveInterval(minutes);
                    AlarmUtil.disableAlarm();
                    AlarmUtil.setNextAlarm(minutes);
                    String toastText = String.format(getString(R.string.updated_from_wear), minutes);
                    Toast toast = Toast.makeText(BaseApplication.APPLICATION, toastText, Toast.LENGTH_SHORT);
                    toast.show();
                    Intent intent = new Intent(BroadcastEvents.INTERVAL_TIME_UPDATED);
                    LocalBroadcastManager.getInstance(BaseApplication.APPLICATION).sendBroadcast(intent);
                    // Consumer shall delete the data when consumed
                    Uri dataItemUri = dataEvent.getDataItem().getUri();
                    new DeleteDataItemCommand(this).execute(dataItemUri);
                }
            }
        }
    }
}
