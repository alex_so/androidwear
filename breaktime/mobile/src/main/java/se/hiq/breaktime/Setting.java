package se.hiq.breaktime;

import android.content.SharedPreferences;

/**
 * Created by alexso on 14/03/15.
 */
public class Setting {
    private final String TIME_INTERVAL_KEY = "se.hiq.breaktime.setting.TIME_INTERVAL_KEY";

    private SharedPreferences preferences;

    public Setting(SharedPreferences preferences) {

        this.preferences = preferences;
    }

    public boolean saveInterval(int interval) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(TIME_INTERVAL_KEY, interval);
        return editor.commit();
    }

    public int getInterval() {
        return preferences.getInt(TIME_INTERVAL_KEY, -1);
    }
}
