package se.hiq.breaktime;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;

import se.hiq.shared.Constants;
import se.hiq.shared.NotificationData;
import se.hiq.shared.SendDataToOtherNodes;


public class MainActivity extends Activity implements View.OnClickListener {

    private int defaultIntervalTime = 60;
    private NumberPicker numberPicker;
    private TextView timerTextView;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BroadcastEvents.INTERVAL_TIME_UPDATED)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                });
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.activity_main_button_set_timer).setOnClickListener(this);
        findViewById(R.id.activity_main_button_disable_timer).setOnClickListener(this);
        findViewById(R.id.activity_main_button_mock_notification).setOnClickListener(this);
        numberPicker = (NumberPicker) findViewById(R.id.activity_main_numberpicker_minutes);
        numberPicker.setMaxValue(1440);
        numberPicker.setMinValue(1);
        timerTextView = (TextView) findViewById(R.id.activity_main_textview_timer_text);
        updateUI();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_button_set_timer:
                onSetTimerClick(v);
                break;
            case R.id.activity_main_button_disable_timer:
                onDisableTimerClick(v);
                break;
            case R.id.activity_main_button_mock_notification:
                NotificationData notificationData = new NotificationData(Constants.NOTIFICATION_ID, getString(R.string.notification_title), getString(R.string.notification_content), Calendar.getInstance().getTimeInMillis(), new String[]{Constants.ACTION_SET_INTERVAL, Constants.ACTION_TURN_OFF});
                new SendDataToOtherNodes(this).execute(notificationData);
                break;
        }
    }

    private void onDisableTimerClick(View view) {
        BaseApplication.APPLICATION.getSetting().saveInterval(-1);
        updateUI();
        AlarmUtil.disableAlarm();
    }

    private void onSetTimerClick(View v) {
        int intervalInMinutes = numberPicker.getValue();
        BaseApplication.APPLICATION.getSetting().saveInterval(intervalInMinutes);
        updateUI();
        AlarmUtil.setNextAlarm(intervalInMinutes);
    }

    private void updateUI() {
        int savedIntervalValue = BaseApplication.APPLICATION.getSetting().getInterval();
        if (savedIntervalValue < 0) {
            timerTextView.setText(R.string.interval_not_set);
            numberPicker.setValue(defaultIntervalTime);
        } else {
            timerTextView.setText(String.format(getString(R.string.interval_set), savedIntervalValue));
            numberPicker.setValue(savedIntervalValue);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BroadcastEvents.INTERVAL_TIME_UPDATED);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }
}
