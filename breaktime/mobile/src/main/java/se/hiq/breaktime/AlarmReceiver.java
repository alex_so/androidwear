package se.hiq.breaktime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import se.hiq.shared.Constants;
import se.hiq.shared.NotificationData;
import se.hiq.shared.SendDataToOtherNodes;

/**
 * Created by alexso on 14/03/15.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, String.format("Timer alarmed"));
        // Send notification to wearable
        NotificationData notificationData = new NotificationData(Constants.NOTIFICATION_ID, context.getString(R.string.notification_title), context.getString(R.string.notification_content), Calendar.getInstance().getTimeInMillis(), new String[]{Constants.ACTION_SET_INTERVAL, Constants.ACTION_TURN_OFF});
        new SendDataToOtherNodes(context).execute(notificationData);
        int intervalInMinutes = BaseApplication.APPLICATION.getSetting().getInterval();
        AlarmUtil.setNextAlarm(intervalInMinutes);
    }
}
