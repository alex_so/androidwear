package se.hiq.breaktime;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import se.hiq.shared.Constants;

/**
 * Created by alexso on 15/03/15.
 */
public class AlarmUtil {

    private final static String TAG = AlarmUtil.class.getSimpleName();
    public static final int ALARM_ID = 1001;

    public static void setNextAlarm(int minutes) {
        Intent intent = new Intent(BaseApplication.APPLICATION, AlarmReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(BaseApplication.APPLICATION, ALARM_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) BaseApplication.APPLICATION.getSystemService(BaseApplication.APPLICATION.ALARM_SERVICE);
        long intervalInMilliseconds = minutes * 60 * 1000;
        long now = Calendar.getInstance().getTimeInMillis();
        long nextAlarmTime = now + intervalInMilliseconds;
        alarmManager.set(AlarmManager.RTC_WAKEUP, nextAlarmTime, pIntent);
        Log.d(TAG, "Next alarm is " + nextAlarmTime);
    }

    public static void disableAlarm() {
        Log.d(TAG, "Cancelling alarm");
        Intent intent = new Intent(BaseApplication.APPLICATION, AlarmReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(BaseApplication.APPLICATION, ALARM_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) BaseApplication.APPLICATION.getSystemService(BaseApplication.APPLICATION.ALARM_SERVICE);
        alarmManager.cancel(pIntent);
    }
}
