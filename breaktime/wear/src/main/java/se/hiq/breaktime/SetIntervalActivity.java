package se.hiq.breaktime;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import se.hiq.shared.Constants;
import se.hiq.shared.IntervalData;
import se.hiq.shared.SendDataToOtherNodes;

public class SetIntervalActivity extends Activity implements WearableListView.ClickListener {

    private List<String> listElements = new ArrayList<String>();
    private WearListAdapter adapter;

    private final String MINUTES_SET = "MinutesSet";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear);
        WearableListView listView = (WearableListView) findViewById(R.id.listView);
        for (int i = 1; i <= 1440; i++) {
            listElements.add(String.valueOf(i));
        }
        adapter = new WearListAdapter(this, listElements);
        listView.setAdapter(adapter);
        listView.setClickListener(this);
    }

    @Override
    public void onClick(WearableListView.ViewHolder v) {
        Integer tag = (Integer) v.itemView.getTag();
        int minutesSet = tag +1;

        Intent intent = new Intent(this, PendingActivity.class);
        intent.putExtra(Constants.FUNCTION_KEY, getString(R.string.title_action_set_interval));
        intent.putExtra(MINUTES_SET, minutesSet);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            int minutes = data.getIntExtra(MINUTES_SET, -1);
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            IntervalData intervalData = new IntervalData(timeStamp, minutes);
            new SendDataToOtherNodes(this).execute(intervalData);
            if (getIntent().getIntExtra(Constants.KEY_NOTIFICATION_ID, -1) != -1) {
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))
                        .cancel(Constants.WEAR_TAG, Constants.NOTIFICATION_ID);
            }
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onTopEmptyRegionClick() {

    }
}
