package se.hiq.breaktime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.wearable.activity.ConfirmationActivity;

/**
 * Created by alexso on 15/03/15.
 */
public class ActivityUtil {

    public static void displayConfirmationActivity(Context context, boolean isSuccess, String title) {
        Intent intent = new Intent(context, ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                isSuccess ? ConfirmationActivity.SUCCESS_ANIMATION : ConfirmationActivity.FAILURE_ANIMATION);
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE, title);
        context.startActivity(intent);
    }

    public static void dismissOwnActivity(Activity activity, boolean animated) {
        if (!animated) {
            activity.overridePendingTransition(0, 0);
        }
        activity.finish();
    }
}