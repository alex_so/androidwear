package se.hiq.breaktime;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import se.hiq.shared.Constants;
import se.hiq.shared.NotificationData;

/**
 * Created by alexso on 14/03/15.
 */
public class WearableNotificationManager {
    private final String TAG = this.getClass().getSimpleName();
    private final Context context;

    public WearableNotificationManager(Context context) {
        this.context = context;
    }

    public void postWearableNotification(int notificationId, NotificationData notificationData) {
        String title = notificationData.getTitle();
        String content = notificationData.getContent();
        String[] actions = notificationData.getActions();

        // TODO: Set another icon.
        //Integer smallIconDrawableId = android.R.drawable.ic_lau;

        Log.v(TAG, String.format("postWearableNotification: %d, title= %s, content = %s, actions=%s", notificationId, title, content, actions.toString()));

        List<NotificationCompat.Action> notificationActions = getNotificationActions(notificationId, notificationData);
        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender()
                .addActions(notificationActions)
                .addAction(getOpenOnPhoneNotificationAction(notificationId, notificationData));
  //              .setBackground(backgroundBitmap);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_plusone_small_off_client)
                .extend(wearableExtender);

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(Constants.WEAR_TAG, notificationId, builder.build());
    }

    private List<NotificationCompat.Action> getNotificationActions(int notificationId, NotificationData notificationData) {
        List<NotificationCompat.Action> notificationActions = new ArrayList<NotificationCompat.Action>();
        for (String action : notificationData.getActions()) {
            PendingIntent pendingActionIntent = getActionPendingIntent(notificationId, action, notificationData.getTimeStamp());

            NotificationCompat.Action notificationAction = new NotificationCompat.Action.Builder(
                    getDrawableIdForAction(action),
                    getTitleForAction(action),
                    pendingActionIntent)
                    .build();

            notificationActions.add(notificationAction);
        }
        return notificationActions;
    }

    private NotificationCompat.Action getOpenOnPhoneNotificationAction(int notificationId, NotificationData notificationData) {
        Intent openIntent = new Intent(Constants.ACTION_OPEN_ON_PHONE);
        openIntent.putExtra(Constants.KEY_NOTIFICATION_ID, notificationId);
        PendingIntent pendingOpenIntent =
                PendingIntent.getService(context, notificationId, openIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Action.Builder(
                R.drawable.go_to_phone_00156,
                context.getString(R.string.notification_title_action_open_on_phone),
                pendingOpenIntent)
                .build();
    }

    private int getDrawableIdForAction(String action) {
        int drawableId;
        if (Constants.ACTION_TURN_OFF.equals(action)) {
            drawableId = R.drawable.common_full_open_on_phone;     // TODO: change icons
        } else if (Constants.ACTION_SET_INTERVAL.equals(action)) {
            drawableId = R.drawable.common_full_open_on_phone;     // TODO: change icons
        } else {
            drawableId = R.drawable.common_full_open_on_phone;     // TODO: change icons
        }
        return drawableId;
    }

    private String getTitleForAction(String action) {
        String title;
        if (Constants.ACTION_TURN_OFF.equals(action)) {
            title = context.getString(R.string.title_action_disable);
        } else if (Constants.ACTION_SET_INTERVAL.equals(action)) {
            title = context.getString(R.string.title_action_set_interval);
        } else {
            title = context.getString(R.string.title_action_default);
        }
        return title;
    }

    private PendingIntent getActionPendingIntent(int notificationId, String action, long timestamp) {
        PendingIntent pendingActionIntent;
        if (Constants.ACTION_SET_INTERVAL.equals(action)) {
            Intent intent = new Intent(context, SetIntervalActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(Constants.KEY_NOTIFICATION_ID, notificationId);
            intent.putExtra(Constants.KEY_NOTIFICATION_TIMESTAMP, timestamp);
            pendingActionIntent =
                    PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        } else {
            Intent actionIntent = new Intent(action);
            actionIntent.putExtra(Constants.KEY_NOTIFICATION_ID, notificationId);
            actionIntent.putExtra(Constants.KEY_NOTIFICATION_TIMESTAMP, timestamp);
            pendingActionIntent =
                    PendingIntent.getService(context, 0, actionIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        return pendingActionIntent;
    }

    public void dismissWearableNotification(int notificationId) {
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                .cancel(Constants.WEAR_TAG, notificationId);
    }
}
