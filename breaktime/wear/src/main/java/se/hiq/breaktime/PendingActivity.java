package se.hiq.breaktime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.DelayedConfirmationView;
import android.view.View;
import android.widget.TextView;

import se.hiq.shared.Constants;

/**
 * Created by alexso on 15/03/15.
 */
public class PendingActivity extends Activity implements DelayedConfirmationView.DelayedConfirmationListener {

    private DelayedConfirmationView mDelayedView;
    boolean isCancelled = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending);
        Intent intent = getIntent();
        final String functionText = intent.getStringExtra(Constants.FUNCTION_KEY);
        mDelayedView = (DelayedConfirmationView) findViewById(R.id.delayed_confirm);
        TextView function = (TextView) findViewById(R.id.textView_functionName_activity_pending);
        function.setText(functionText);
        mDelayedView.setListener(PendingActivity.this);
        mDelayedView.setTotalTimeMs(2000);
        mDelayedView.start();
    }

    @Override
    public void onTimerFinished(View view) {
        if (isCancelled) {
            return;
        }
        setResult(RESULT_OK, getIntent());
        ActivityUtil.displayConfirmationActivity(this, true, getString(R.string.command_sent));
        ActivityUtil.dismissOwnActivity(this, false);
    }

    @Override
    public void onTimerSelected(View view) {
        setResult(RESULT_CANCELED);
        isCancelled = true;
        ActivityUtil.dismissOwnActivity(this, false);
    }
}
