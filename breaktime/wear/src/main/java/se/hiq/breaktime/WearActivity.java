package se.hiq.breaktime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;

import java.util.ArrayList;
import java.util.List;

import se.hiq.shared.Constants;
import se.hiq.shared.SendMessageToOtherNodes;

public class WearActivity extends Activity implements WearableListView.ClickListener {

    private List<String> listElements = new ArrayList<String>();
    private WearListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear);
        WearableListView listView = (WearableListView) findViewById(R.id.listView);
        listElements.add("Set interval");
        listElements.add("Disable notifications");
        adapter = new WearListAdapter(this, listElements);
        listView.setAdapter(adapter);
        listView.setClickListener(this);
    }

    @Override
    public void onClick(WearableListView.ViewHolder v) {
        Integer tag = (Integer) v.itemView.getTag();
        switch (tag) {
            case 0:
                startActivity(new Intent(this, SetIntervalActivity.class));
                break;
            case 1:
                Intent intent = new Intent(this, PendingActivity.class);
                intent.putExtra(Constants.FUNCTION_KEY, getString(R.string.title_action_disable));
                startActivityForResult(intent, 0);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            new SendMessageToOtherNodes(this).execute(Constants.DISABLE_NOTIFICATIONS_PATH);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onTopEmptyRegionClick() {

    }
}
