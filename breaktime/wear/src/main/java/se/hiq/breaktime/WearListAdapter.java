package se.hiq.breaktime;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by alexso on 15/03/15.
 */
public class WearListAdapter extends WearableListView.Adapter {
    List<String> listItems;
    private final Context context;
    private final LayoutInflater inflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public WearListAdapter(Context context, List<String> listItems) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.listItems = listItems;
    }

    // Provide a reference to the type of views you're using
    public static class ItemViewHolder extends WearableListView.ViewHolder {
        private TextView textView;
        public ItemViewHolder(View itemView) {
            super(itemView);
            // find the text view within the custom item's layout
            textView = (TextView) itemView.findViewById(android.R.id.text1);
        }
    }

    // Create new views for list items
    // (invoked by the WearableListView's layout manager)
    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // Inflate our custom layout for list items
        return new ItemViewHolder(inflater.inflate(android.R.layout.simple_list_item_1, null));
    }

    // Replace the contents of a list item
    // Instead of creating new views, the list tries to recycle existing ones
    // (invoked by the WearableListView's layout manager)
    @Override
    public void onBindViewHolder(WearableListView.ViewHolder holder,
                                 int position) {
        ItemViewHolder itemHolder = (ItemViewHolder) holder;
        TextView view = itemHolder.textView;
        view.setTextColor(context.getResources().getColor(R.color.black));
        view.setText(listItems.get(position));
        view.setTag(position);
    }

    // Return the size of your dataset
    // (invoked by the WearableListView's layout manager)
    @Override
    public int getItemCount() {
        return listItems.size();
    }
}
