package se.hiq.breaktime;

import android.app.IntentService;
import android.content.Intent;
import android.support.wearable.activity.ConfirmationActivity;

import se.hiq.shared.Constants;
import se.hiq.shared.SendMessageToOtherNodes;

/**
 * Created by alexso on 15/03/15.
 */

/**
 * An {@link android.app.IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class NotificationActionIntentService extends IntentService {

    private WearableNotificationManager wearableNotificationManager;

    public NotificationActionIntentService() {
        super("NotificationActionIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        wearableNotificationManager = new WearableNotificationManager(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (Constants.ACTION_TURN_OFF.equals(action)) {
                displaySuccessConfirmation(getString(R.string.command_sent));
                new SendMessageToOtherNodes(this).execute(Constants.DISABLE_NOTIFICATIONS_PATH);
                wearableNotificationManager.dismissWearableNotification(Constants.NOTIFICATION_ID);
            } else if (Constants.ACTION_OPEN_ON_PHONE.equals(action)) {
                displayOpenOnPhoneConfirmation();
                new SendMessageToOtherNodes(this).execute(Constants.START_ACTIVITY_PATH);
                wearableNotificationManager.dismissWearableNotification(Constants.NOTIFICATION_ID);
            }
        }
    }

    private void displayOpenOnPhoneConfirmation() {
        Intent openOnPhoneIntent = new Intent(this, ConfirmationActivity.class);
        openOnPhoneIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        openOnPhoneIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.OPEN_ON_PHONE_ANIMATION);
        startActivity(openOnPhoneIntent);
    }

    private void displaySuccessConfirmation(String message) {
        Intent confirmationIntent = new Intent(this, ConfirmationActivity.class);
        confirmationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        confirmationIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
        if (message != null) {
            confirmationIntent.putExtra(ConfirmationActivity.EXTRA_MESSAGE, message);
        }
        startActivity(confirmationIntent);
    }


}

