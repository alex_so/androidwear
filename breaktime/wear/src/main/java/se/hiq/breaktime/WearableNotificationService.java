package se.hiq.breaktime;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import se.hiq.shared.Constants;
import se.hiq.shared.NotificationData;

/**
 * Created by alexso on 14/03/15.
 */
public class WearableNotificationService extends WearableListenerService {

    private final WearableNotificationManager wearableNotificationManager;

    public WearableNotificationService() {
        wearableNotificationManager = new WearableNotificationManager(this);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent dataEvent : dataEvents) {
            String path = dataEvent.getDataItem().getUri().getPath();
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                if (path.startsWith(Constants.NOTIFICATION_PATH)) {
                    int notificationId = Integer.parseInt(path.substring(Constants.NOTIFICATION_PATH.length() + 1));

                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    String title = dataMapItem.getDataMap().getString(Constants.KEY_NOTIFICATION_TITLE);
                    String content = dataMapItem.getDataMap().getString(Constants.KEY_NOTIFICATION_CONTENT);
                    long timestamp = dataMapItem.getDataMap().getLong(Constants.KEY_NOTIFICATION_TIMESTAMP);
                    String[] actions = dataMapItem.getDataMap().getStringArray(Constants.KEY_NOTIFICATION_ACTIONS);


                    NotificationData notificationData = new NotificationData(notificationId, title, content, timestamp, actions);
                    wearableNotificationManager.postWearableNotification(notificationId, notificationData);
                }
            } else if (dataEvent.getType() == DataEvent.TYPE_DELETED) {
                if (path.startsWith(Constants.NOTIFICATION_PATH)) {
                    int notificationId = Integer.parseInt(path.substring(Constants.NOTIFICATION_PATH.length() + 1));
                    wearableNotificationManager.dismissWearableNotification(notificationId);
                }
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        Log.v("WearableNot...Service", "onMessageReceived: " + messageEvent.getPath());
    }
}
