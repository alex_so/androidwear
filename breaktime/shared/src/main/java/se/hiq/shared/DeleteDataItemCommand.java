package se.hiq.shared;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by alexso on 15/03/15.
 */
public class DeleteDataItemCommand implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<DataApi.DeleteDataItemsResult>
{
    private static final String TAG = DeleteDataItemCommand.class.getSimpleName();
    private final GoogleApiClient googleApiClient;
    private Uri uri;

    public DeleteDataItemCommand(Context context)
    {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void execute(Uri uri)
    {
        this.uri = uri;
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        Wearable.DataApi.deleteDataItems(googleApiClient, uri).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i)
    {
        Log.d(TAG, "onConnectionSuspended: Reason " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    @Override
    public void onResult(DataApi.DeleteDataItemsResult deleteDataItemsResult)
    {
        if (!deleteDataItemsResult.getStatus().isSuccess())
        {
            Log.e(TAG, "onResult: Failed to delete dataItem " + deleteDataItemsResult);
        }
        googleApiClient.disconnect();
    }
}
