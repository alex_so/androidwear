package se.hiq.shared;

/**
 * Created by alexso on 28/03/15.
 */
public class IntervalData {
    private long timeStamp;
    private int minutes;

    public IntervalData(long timeStamp, int minutes) {
        this.timeStamp = timeStamp;
        this.minutes = minutes;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public int getMinutes() {
        return minutes;
    }
}
