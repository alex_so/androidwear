package se.hiq.shared;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.HashSet;

/**
 * Created by alexso on 15/03/15.
 */
public class SendMessageToOtherNodes implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = this.getClass().getSimpleName();
    private final GoogleApiClient googleApiClient;
    private String message;

    public SendMessageToOtherNodes(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void execute(String message) {
        this.message = message;
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        new SendMessageToConnectedNodesAsyncTask().execute(message);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: Reason " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    private class SendMessageToConnectedNodesAsyncTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            if (params.length != 1) {
                Log.e(TAG, "sendMessageToConnectedNodesAsyncTask: Wrong number of arguments to execute.");
                return null;
            }

            HashSet<String> nodeIds = new HashSet<String>();
            NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi
                    .getConnectedNodes(googleApiClient)
                    .await();

            if (!nodesResult.getStatus().isSuccess()) {
                Log.e(TAG, "sendMessageToConnectedNodesAsyncTask: Failed to get nodes " + nodesResult);
            } else {
                for (Node node : nodesResult.getNodes()) {
                    nodeIds.add(node.getId());
                }

                for (String nodeId : nodeIds) {
                    MessageApi.SendMessageResult sendMessageResult = Wearable.MessageApi
                            .sendMessage(googleApiClient, nodeId, params[0], null)
                            .await();

                    if (!sendMessageResult.getStatus().isSuccess()) {
                        Log.e(TAG, "sendMessageToConnectedNodesAsyncTask: Failed to send message " + sendMessageResult);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            googleApiClient.disconnect();
        }
    }
}

