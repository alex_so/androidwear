package se.hiq.shared;

/**
 * Created by alexso on 14/03/15.
 */
public class Constants {
    // Path for messages
    public static final String NOTIFICATION_PATH = "/notification";
    public static final String SET_INTERVAL_PATH = "/set/interval/";
    public static final String DISABLE_NOTIFICATIONS_PATH = "/disable";
    public static final String START_ACTIVITY_PATH = "/start/activity";

    // DataType (for converting data type into a dataItem)
    public static final int NOTIFICATION_ID = 1001;
    public static final String KEY_NOTIFICATION_ID = "KEY_NOTIFICATION_ID";
    public static final String KEY_NOTIFICATION_TITLE = "KEY_NOTIFICATION_TITLE";
    public static final String KEY_NOTIFICATION_CONTENT = "KEY_NOTIFICATION_CONTENT";
    public static final String KEY_NOTIFICATION_TIMESTAMP = "KEY_NOTIFICATION_TIMESTAMP";
    public static final String KEY_NOTIFICATION_ACTIONS = "KEY_NOTIFICATION_ACTIONS";
    public static final String KEY_SET_INTERVAL_VALUE = "KEY_SET_INTERVAL_VALUE";

    // Actions done on Wear and sent to Handheld
    public static final String ACTION_TURN_OFF = "se.hiq.shared.action.TURN_OFF";
    public static final String ACTION_SET_INTERVAL = "se.hiq.shared.action.SET_INTERVAL";
    public static final String ACTION_OPEN_ON_PHONE = "se.hiq.shared.action.OPEN_ON_PHONE";

    // Notification TAG -- only used by wear
    public static final String WEAR_TAG = "wear";

    // Intent string name. Only used by Wear
    public static final String FUNCTION_KEY = "functionKey" ;

}
