package se.hiq.shared;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.Calendar;

/**
 * Created by alexso on 15/03/15.
 */
public class SendDataToOtherNodes implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<DataApi.DataItemResult> {
    private final String TAG = this.getClass().getSimpleName();
    private final GoogleApiClient googleApiClient;
    private NotificationData notificationData;
    private IntervalData intervalData;
    private DataType dataType;

    public SendDataToOtherNodes(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void execute(IntervalData intervalData) {
        dataType = DataType.SetInterval;
        this.intervalData = intervalData;
        googleApiClient.connect();
    }

    public void execute(NotificationData notificationData)
    {
        dataType = DataType.Notification;
        this.notificationData = notificationData;
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        PutDataMapRequest dataMapRequest;
        switch (dataType) {
            case Notification:
                dataMapRequest = PutDataMapRequest.create(Constants.NOTIFICATION_PATH + "/" + Constants.NOTIFICATION_ID);
                dataMapRequest.getDataMap().putString(Constants.KEY_NOTIFICATION_TITLE, notificationData.getTitle());
                dataMapRequest.getDataMap().putString(Constants.KEY_NOTIFICATION_CONTENT, notificationData.getContent());
                dataMapRequest.getDataMap().putLong(Constants.KEY_NOTIFICATION_TIMESTAMP, Calendar.getInstance().getTimeInMillis());
                dataMapRequest.getDataMap().putStringArray(Constants.KEY_NOTIFICATION_ACTIONS, notificationData.getActions());
                break;
            case SetInterval:
                dataMapRequest = PutDataMapRequest.create(Constants.SET_INTERVAL_PATH);
                dataMapRequest.getDataMap().putLong(Constants.KEY_NOTIFICATION_TIMESTAMP, intervalData.getTimeStamp());
                dataMapRequest.getDataMap().putInt(Constants.KEY_SET_INTERVAL_VALUE, intervalData.getMinutes());
                break;
            default:
                assert false : "Invalid data item";
                return;
        }
        PutDataRequest dataRequest = dataMapRequest.asPutDataRequest();
        Wearable.DataApi.putDataItem(googleApiClient, dataRequest).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: Reason " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    @Override
    public void onResult(DataApi.DataItemResult dataItemResult) {
        Log.d(TAG, "onResult: " + dataItemResult.getStatus());
        if (!dataItemResult.getStatus().isSuccess()) {
            Log.e(TAG, "onResult: Failed to put dataItem " + dataItemResult);
        }

        googleApiClient.disconnect();
    }
}
