package se.hiq.shared;

/**
 * Created by alexso on 28/03/15.
 */
public enum DataType {
    Notification,
    SetInterval
}
