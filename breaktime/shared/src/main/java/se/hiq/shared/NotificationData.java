package se.hiq.shared;

/**
 * Created by alexso on 14/03/15.
 */
public class NotificationData {

    private final int notificationId;
    private final String title;
    private final String content;
    private final long timeStamp;
    private final String[] actions;

    public NotificationData(int notificationId, String title, String content, long timeStamp, String[] actions) {
        this.notificationId = notificationId;
        this.title = title;
        this.content = content;
        this.timeStamp = timeStamp;
        this.actions = actions;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String[] getActions() {
        return actions;
    }
}
